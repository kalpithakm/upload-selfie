import React, { Component } from "react";
import "./App.css";
import "./custom.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button, Input } from "reactstrap";
import  "bootstrap/dist/css/bootstrap.css";
import { Cookies } from "react-cookie";
import Header from './Header';
const cookies = new Cookies();
var date = new Date();

class Show extends Component {
 
  constructor(props) {
    super(props);

    var AuthCookie = localStorage.getItem("cookieToken");
 console.log(AuthCookie);

     this.state = {
        apikey: cookies.get('cookieToken')
     }

  var x = cookies.get('cookieToken');
  /*var y = x.key;
  var success = y.replace("Token ", '');

  console.log("y", y);
  console.log("success", success);


     window.onload = function(){
     document.getElementById("apikey").value = success;
     }   */  
     
  }



  onCameraError(error) {
    console.error("onCameraError");
    document.getElementById("video-section").style.display = "none";
  }

  onCameraStart(stream) {
    console.log("onCameraStart");
  }

  onCameraStop() {
    console.log("onCameraStop");
  }

  componentDidMount(){

      const script = document.createElement("script");

    script.src = "https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js";
    script.async = true;

    document.body.appendChild(script);
    
 jQuery("#createlist").click(function() {

   var videoapikey = document.getElementById("apikey").value;
        console.log(videoapikey);
        var str = "Token";
       var imgapikey =  str + " " + videoapikey;

   jQuery.ajax({
                url:
                  "https://southeastasia.cognitive.sparshik.com/api/v1/facelists/",
                headers: {
                  Authorization:
                    imgapikey,
                  "Content-Type": "application/json"
                },
                method: "GET",
                crossDomain: true,
                credentials: "include",

                success: function(response, status, xhr) {
                  if (response) {
                      jQuery("#error-message").hide();
                    document.getElementById("idn-tab").style.display = "none";
                    document.getElementById("errorresponsetab").style.display = "none";
                    document.getElementById("responsetab").style.display = "block";

                    var codeString = JSON.stringify(response, null, 4);
                            jQuery('#responsecode').html(codeString);
                    console.log(codeString);

             var cookieSetData = xhr.getResponseHeader('Set-Cookie');
             var allHeader = xhr.getAllResponseHeaders();
            console.log("after cookies:" + document.cookie);


 var minutes = 30;
 date.setTime(date.getTime() + (minutes * 60 * 1000));
          cookies.set('cookieToken', {key: imgapikey})
 console.log("new cookies:", cookies);



                    document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";
                  
                  
                  }
                },

                  error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("idn-tab").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }
              });
    });



      }



  render() {
    return (

      <div className="Show">
         <Header />

        <div id="identify-namepath">
        <Row>
           <Col>
           </Col>

           <Col>
          
           <div className="idn-list" id="idn-tab">
            <span className="idn-input">
                        <label>API Key</label>
                        <Input type="text" id="apikey"></Input>
            </span>

             <Button id="createlist"> Please Click Here to show all Lists </Button>
             </div>

              <span className="idn-input" style={{ display: "none" }}> 
             <label>Face List UID</label>      
              <Input type="text" id="listname">
                </Input>
            </span>

                 </Col>

                 <Col>
                </Col>

                </Row>
        </div>


          <div id="video-section" style={{ display: "none" }}>
          <Camera
            onTakePhoto={dataUri => {
              this.onTakePhoto(dataUri);
            }}
            onCameraError={error => {
              this.onCameraError(error);
            }}
            idealFacingMode={FACING_MODES}
            idealResolution={{ width: 900, height: 500 }}
            imageType={IMAGE_TYPES.JPG}
            imageCompression={0.50}
            isMaxResolution={false}
            isImageMirror={false}
            isDisplayStartCameraError={true}
            sizeFactor={1}
            onCameraStart={stream => {
              this.onCameraStart(stream);
            }}
            onCameraStop={() => {
              this.onCameraStop();
            }}
          />
       
         <div className="loader"> </div>

          <div id="tips">
          <p>
           Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears.          
          </p>
     
          </div>
          <div id="options-or">
            <p className="or">OR</p>
          </div>
        </div>

        <div id="upload-photo-section" style={{ display: "none" }}>
          <div className="ios-pic">
            <div>
              <img className="display-img" id="pic" src="" alt=""/>
              <img className="display-img-url" id="photo" src="" alt="" />
            </div>

            <div className="upload-loader">  </div>

            <div>
              <label className="fileContainer">
                Use a Photo
                <input
                  id="browse-file"
                  type="file"
                  onClick={this.uploadSelfie}
                />
              </label>
             
            </div>
           
          </div>
        </div>


       <div>
        <p id="error-message" />
         </div>

          <div>
        <p id="success-message" />
         </div>

         <div id="responsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
                
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
         <Col></Col>
         <Col>
               <span className = "try-tab">
                     <a href="/#/show" onClick={() => window.location.reload()}>Back</a>
                </span>
          </Col>
          <Col></Col>
         </Row>
    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
                 
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/show" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>

    </div>

       
      </div>
    );
  }
}

export default Show;