import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import Camera, {
    FACING_MODES,
    IMAGE_TYPES
} from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import {
    Row,
    Col,
    Input, Button
} from "reactstrap";
import {
    Link
} from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.css";
import loader from './loader.gif';
import Header from './Header';
import { Cookies } from "react-cookie";
import Identify from './Identify';
const cookies = new Cookies();
var APIkey, dataUri1, dataUri2;


class Compare extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);
         this.uploadSelfiesecond = this.uploadSelfiesecond.bind(this);

      this.state = {
        apikey: cookies.get('cookieAPIkey')
     }
    var myCookie = cookies.get('cookieAPIkey')

    if (myCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
   console.log("APIkey", APIkey);
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }
  }

   onTakePhoto(dataUri) {
     
        console.log("takePhoto");
    }

    uploadSelfie(e) {
        jQuery(function($) {
            function readFile() {
              document.getElementById("errorresponsetab").style.display = "none";
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();
                    ImgfileReader.addEventListener("load", function(e) {
                    document.getElementById("image-box").style.display = "block";
                        document.getElementById("photo").src = e.target.result;
                         dataUri1 = e.target.result; 
                        console.log("dataUri2", dataUri1);
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
  }

     uploadSelfiesecond(e) {
          jQuery(function($) {
            function readFile() {
                document.getElementById("errorresponsetab").style.display = "none";
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();
                    ImgfileReader.addEventListener("load", function(e) {
                    document.getElementById("image-box-1").style.display = "block";
                        document.getElementById("photo-1").src = e.target.result;
                         dataUri2 = e.target.result;
                        console.log("dataUri2",dataUri2);

                    });
                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }
            document
                .getElementById("browse-file-2")
                .addEventListener("change", readFile);
        });
      }

    componentDidMount(){
 jQuery("#submitselfie").click(function() {

     console.log("dataUri1",dataUri1);
     console.log("dataUri2",dataUri2);
 
                        jQuery.ajax({
                            url: "https://southeastasia.cognitive.sparshik.com/api/v1/face/compare/image/",
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: APIkey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri1: dataUri1,
                                dataUri2: dataUri2

                            }),

                            success: function(response) {
                                jQuery("#error-message").hide();
                                console.log(response);

                                if (response) {
                                    document.getElementById("idn-tab").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("submitselfie").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                   
                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader-1').show();
                               

                            },
                            complete: function() {
                                jQuery('.upload-loader-1').hide();
                               
                            },
                            error:function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                
                                document.getElementById("idn-tab").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                

                            }
                        });
 });
 }    

  onCameraError(error) {
        console.error("onCameraError");
        document.getElementById("video-section").style.display = "none";
        this.uploadSelfie();
    }

    onCameraStart(stream) {
        console.log("onCameraStart");
    }

    onCameraStop() {
        console.log("onCameraStop");
    }
    

    render() {
        return ( 
     <div className="Compare">
    <Header />

    <div className="page-title">
    <h5> Compare the Face / Documents</h5>
    </div>

       <div id="video-section" style={{ display: "none" }}>
        <Camera
            onTakePhoto={dataUri => {
              this.onTakePhoto(dataUri);
            }}
            onCameraError={error => {
              this.onCameraError(error);
            }}
            idealFacingMode={FACING_MODES}
            idealResolution={{ width: 900, height: 500 }}
            imageType={IMAGE_TYPES.JPG}
            imageCompression={0.50}
            isMaxResolution={false}
            isImageMirror={false}
            isDisplayStartCameraError={true}
            sizeFactor={1}
            onCameraStart={stream => {
              this.onCameraStart(stream);
            }}
            onCameraStop={() => {
              this.onCameraStop();
            }}
          />
        <div className="loader"></div>
        <div id="tips">
            <p>
           Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears.          
          </p>
        </div>
        <div id="options-or">
            <p className="or">OR</p>
        </div>
        </div>
    
    <div id="Compare-namepath">
        <Row>
            <Col></Col>
            <Col>
                <div className="idn-list" id="idn-tab">
                   
                    
                </div>
            </Col>
            <Col></Col>
        </Row>
    </div>

    
 
 <div className="container"> 
<Row className="compare-photo-tab">
            <Col>
   
    <div id="upload-photo-section">
        <div className="ios-pic">
            <div className="p-border" id="image-box" style={{ display: "none" }}>
                <img className="display-img" id="pic" src="" alt=""/>
                <img className="display-img-url" id="photo" src="" alt="" />
            </div>
            <div>
                <label className="fileContainer">
                Upload Document 1
                
                    <input
                  id="browse-file"
                  type="file"
                  onClick={this.uploadSelfie}
                />
                </label>
            </div>
        </div>
    </div>
    </Col>

     <Col>
     <div id="upload-photo-section">
        <div className="ios-pic">
            <div className="p-border" id="image-box-1" style={{ display: "none" }}>
                <img className="display-img" id="pic" src="" alt=""/>
                <img className="display-img-url" id="photo-1" src="" alt="" />
            </div>
           
            <div>
                <label className="fileContainer">
                Upload Document 2
                
                    <input
                  id="browse-file-2"
                  type="file"
                  onClick={this.uploadSelfiesecond}
                />
                </label>
            </div>
        </div>
    </div></Col>
        </Row>
        </div>


        <Row>
            <Col></Col>
            <Col>
             <div className="upload-loader-1"></div>
            <Button id="submitselfie"> Compare </Button>
            </Col>
            <Col></Col>
        </Row>

    <div>
        <p id="error-message" />
    </div>
     <div>
        <p id="success-message" />
    </div>
    <div id="responsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
            <h5 id="success-message">Success Response</h5>
                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
         <Col></Col>
         <Col>
               <span className = "try-tab">
                     <a href="/#/compare" onClick={() => window.location.reload()}>Back</a>
                </span>
          </Col>
          <Col></Col>
         </Row>

    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
             <h5 id="error-message">Error Response</h5>
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/compare" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

    <div id="try-btn" style={{ display: "none" }}>
        <span className = "try-tab">
            <a href="/">Try Again</a>
        </span>
    </div>
</div>
    );
  }
}

export default Compare;