import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import Camera, {
    FACING_MODES,
    IMAGE_TYPES
} from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import {
    Row,
    Col,
    Input
} from "reactstrap";
import {
    Link
} from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.css";
import loader from './loader.gif';
import Header from './Header';
import { Cookies } from "react-cookie";
const cookies = new Cookies();
var ListUID,APIkey;

class Identify extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);

        this.state = {
        listname: cookies.get('cookieListUID'),
        apikey: cookies.get('cookieAPIkey')
     }

         var myapiCookie = cookies.get('cookieAPIkey')

    if (myapiCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
   console.log("APIkey", APIkey);
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }

  var myCookie = cookies.get('cookieListUID')
    if (myCookie != null) 
    {
  var x = cookies.get('cookieListUID');
   ListUID = x.key;
   console.log("ListUID", ListUID);
    }
   else
   {
     alert("Please enter ListUID");
     window.location.href='/#/editlist';
   }
  }

    onTakePhoto(dataUri) {
        // Do stuff with the dataUri photo...
        console.log(ListUID);

        console.log("takePhoto");
        jQuery.ajax({
            url: "https://southeastasia.cognitive.sparshik.com/api/v1/face/identify/",
            method: "POST",
            crossDomain: true,
            headers: {
                Authorization: APIkey,
                "Content-Type": "application/json"
            },
            data: JSON.stringify({
                dataUri: dataUri,
                listUID: ListUID
            }),
            success: function(response) {
                jQuery("#error-message").hide();
                console.log(response);

                 if (response) 
                 {
                                    document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("idn-tab").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";
                                }


            },

            beforeSend: function() {
                jQuery('.loader').show();
                jQuery('#container-circles').hide();
                jQuery('.react-html5-camera-photo').hide();

            },
            complete: function() {
                jQuery('.loader').hide();
                jQuery('#container-circles').show();
                jQuery('.react-html5-camera-photo').show();
            },

            error:function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("idn-tab").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }
        });
    }

    uploadSelfie(e) {
        document.getElementById("video-section").style.display = "none";
        document.getElementById("tips").style.display = "none";
        document.getElementById("options-or").style.display = "none";

        console.log(ListUID);


        jQuery(function($) {
            function readFile() {
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();

                    ImgfileReader.addEventListener("load", function(e) {
                        document.getElementById("photo").src = e.target.result;
                        var dataUri = e.target.result;

                        jQuery.ajax({
                            url: "https://southeastasia.cognitive.sparshik.com/api/v1/face/identify/",
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: APIkey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri: dataUri,
                                listUID: ListUID,
                                attributes: "age,gender,smoker,ethnicity,alcohol,bmi,region,landmarks"

                            }),

                            success: function(response) {
                                jQuery("#error-message").hide();
                                console.log(response);

                                if (response) {
                                    document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("idn-tab").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";
                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader').show();
                                jQuery('#container-circles').hide();
                                jQuery('.react-html5-camera-photo').hide();

                            },
                            complete: function() {
                                jQuery('.upload-loader').hide();
                                jQuery('#container-circles').show();
                                jQuery('.react-html5-camera-photo').show();
                            },
                            error:function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("idn-tab").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }
                        });
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
    }



    onCameraError(error) {
        console.error("onCameraError");
        document.getElementById("video-section").style.display = "none";
        this.uploadSelfie();
    }

    onCameraStart(stream) {
        console.log("onCameraStart");
    }

    onCameraStop() {
        console.log("onCameraStop");
    }

    render() {
        return ( 
<div className="Identify">
    <Header />
    
    <div id="identify-namepath">
        <Row>
            <Col></Col>
            <Col>
                <div className="idn-list" id="idn-tab">
                   
                    
                </div>
            </Col>
            <Col></Col>
        </Row>
    </div>
    <div id="video-section">
        <Camera
            onTakePhoto={dataUri => {
              this.onTakePhoto(dataUri);
            }}
            onCameraError={error => {
              this.onCameraError(error);
            }}
            idealFacingMode={FACING_MODES}
            idealResolution={{ width: 900, height: 500 }}
            imageType={IMAGE_TYPES.JPG}
            imageCompression={0.50}
            isMaxResolution={false}
            isImageMirror={false}
            isDisplayStartCameraError={true}
            sizeFactor={1}
            onCameraStart={stream => {
              this.onCameraStart(stream);
            }}
            onCameraStop={() => {
              this.onCameraStop();
            }}
          />
        <div className="loader"></div>
        <div id="tips">
            <p>
           Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears.          
          </p>
        </div>
        <div id="options-or">
            <p className="or">OR</p>
        </div>
    </div>
    <div id="upload-photo-section">
        <div className="ios-pic">
            <div>
                <img className="display-img" id="pic" src="" alt=""/>
                <img className="display-img-url" id="photo" src="" alt="" />
            </div>
            <div className="upload-loader"></div>
            <div>
                <label className="fileContainer">
                Use a Photo
                
                    <input
                  id="browse-file"
                  type="file"
                  onClick={this.uploadSelfie}
                />
                </label>
            </div>
        </div>
    </div>
    <div>
        <p id="error-message" />
    </div>
     <div>
        <p id="success-message" />
    </div>
    <div id="responsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
         <Col></Col>
         <Col>
               <span className = "try-tab">
                     <a href="/#/identify" onClick={() => window.location.reload()}>Back</a>
                </span>
          </Col>
          <Col></Col>
         </Row>

    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/identify" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

    <div id="try-btn" style={{ display: "none" }}>
        <span className = "try-tab">
            <a href="/">Try Again</a>
        </span>
    </div>
</div>
    );
  }
}

export default Identify;