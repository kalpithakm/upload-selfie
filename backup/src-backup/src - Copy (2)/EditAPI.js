import React, { Component } from "react";
import "./App.css";
import "./custom.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button, Input } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import Header from './Header';
import { Cookies } from "react-cookie";
const cookies = new Cookies();
var APIkey;

class EditAPI extends Component {
  constructor(props) {
    super(props);

      this.state = {
        apikey: cookies.get('cookieAPIkey')
     }


}



  onCameraError(error) {
    console.error("onCameraError");
    document.getElementById("video-section").style.display = "none";
  }

  onCameraStart(stream) {
    console.log("onCameraStart");
  }

  onCameraStop() {
    console.log("onCameraStop");
  }

  componentDidMount(){
 jQuery("#createlist").click(function() {

  var apikeyvalue = document.getElementById("apikey").value;
    console.log(apikeyvalue);
     var str = "Token";
    var imgapikey =  str + " " + apikeyvalue;
    alert("API Key Saved")
     window.location.href='/';
var CookieDate = new Date;
CookieDate.setFullYear(CookieDate.getFullYear() +10);
var expdate = CookieDate.toGMTString();
    console.log(expdate);
 cookies.set('cookieAPIkey', {key: imgapikey}, { path: '/' }, { expires: expdate })
    });

      }


  render() {
    return (
      <div className="EditAPI">
        <Header />
        
        <div id="identify-namepath">
        <Row>
           <Col>
           </Col>

           <Col>
          
           <div className="idn-list" id="idn-tab">

             <span className="idn-input">
                <label>Please Enter API Key</label>
                 <Input type="text" id="apikey"></Input>
              </span>

              <div>
            <Button id="createlist"> Save </Button>
              </div>

              <div className="alert alert-success" id="alertbox" style={{ display: "none" }}>
                 <strong>Success!</strong> Indicates a successful or positive action.

              </div>

             </div>
                 </Col>

                 <Col>
                </Col>

                </Row>
        </div>


          <div id="video-section" style={{ display: "none" }}>
          <Camera
            onTakePhoto={dataUri => {
              this.onTakePhoto(dataUri);
            }}
            onCameraError={error => {
              this.onCameraError(error);
            }}
            idealFacingMode={FACING_MODES}
            idealResolution={{ width: 900, height: 500 }}
            imageType={IMAGE_TYPES.JPG}
            imageCompression={0.50}
            isMaxResolution={false}
            isImageMirror={false}
            isDisplayStartCameraError={true}
            sizeFactor={1}
            onCameraStart={stream => {
              this.onCameraStart(stream);
            }}
            onCameraStop={() => {
              this.onCameraStop();
            }}
          />
       
         <div className="loader"> </div>

          <div id="tips">
          <p>
           Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears.          
          </p>
     
          </div>
          <div id="options-or">
            <p className="or">OR</p>
          </div>
        </div>

        <div id="upload-photo-section" style={{ display: "none" }}>
          <div className="ios-pic">
            <div>
              <img className="display-img" id="pic" src="" alt=""/>
              <img className="display-img-url" id="photo" src="" alt="" />
            </div>

            <div className="upload-loader">  </div>

            <div>
              <label className="fileContainer">
                Use a Photo
                <input
                  id="browse-file"
                  type="file"
                  onClick={this.uploadSelfie}
                />
              </label>
             
            </div>
           
          </div>
        </div>


         <div>
        <p id="error-message" />
         </div>

         <div>
        <p id="success-message" />
        </div>

         <div id="responsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
         <Col></Col>
         <Col>
               <span className = "try-tab">
                     <a href="/#/list" onClick={() => window.location.reload()}>Back</a>
                </span>
          </Col>
          <Col></Col>
         </Row>
    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/list" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

       
      </div>
    );
  }
}

export default EditAPI;