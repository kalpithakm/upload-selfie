import React, { Component } from "react";
import "./App.css";
import "./custom.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button, Input } from "reactstrap";
import  "bootstrap/dist/css/bootstrap.css";
import Header from './Header';
import { Cookies } from "react-cookie";
const cookies = new Cookies();
var APIkey;

class Show extends Component {
 
  constructor(props) {
    super(props);

    var AuthCookie = localStorage.getItem("cookieToken");
 console.log(AuthCookie);

      this.state = {
        apikey: cookies.get('cookieAPIkey')
     }
    var myCookie = cookies.get('cookieAPIkey')

    if (myCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
   console.log("APIkey", APIkey);
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }  
     
  }



  onCameraError(error) {
    console.error("onCameraError");
    document.getElementById("video-section").style.display = "none";
  }

  onCameraStart(stream) {
    console.log("onCameraStart");
  }

  onCameraStop() {
    console.log("onCameraStop");
  }

  componentDidMount(){

      const script = document.createElement("script");

    script.src = "https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js";
    script.async = true;

    document.body.appendChild(script);
    
 jQuery("#createlist").click(function() {
console.log(APIkey);
   jQuery.ajax({
                url:
                  "https://southeastasia.cognitive.sparshik.com/api/v1/facelists/",
                headers: {
                  Authorization:
                    APIkey,
                  "Content-Type": "application/json"
                },
                method: "GET",
                crossDomain: true,
                credentials: "include",

                success: function(response, status, xhr) {
                   document.getElementById("idn-tab").style.display = "none";
                    document.getElementById("errorresponsetab").style.display = "none";
                    document.getElementById("responsetab").style.display = "block";

                   if (response.lists)
                    {
                    var jString = JSON.stringify(response.lists);
                    var jdata = JSON.parse(jString);
                     console.log(jdata);
                     var trHTML = '';
                   for(var i = 0; i <  jdata.length; i++)
                       {   
                 trHTML += 
                   '<tr><td>' +  jdata[i].listUID + 
                   '</td><td>' + jdata[i].facesCount +
                   '</td><td>' + jdata[i].owner + 
                   '</td><td>' + jdata[i].created +
                   '</td></tr>';            
                      };
                   jQuery('#records_table').append(trHTML);
    
                  }
                },

                  error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("idn-tab").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }
              });
    });



      }



  render() {
    return (

      <div className="Show">
         <Header />

        <div id="identify-namepath">
        <Row>
           <Col>
           </Col>

           <Col>
          
           <div className="idn-list" id="idn-tab">
         

             <Button id="createlist"> Please Click Here to show all Lists </Button>
             </div>


                 </Col>

                 <Col>
                </Col>

                </Row>
        </div>


          <div id="video-section" style={{ display: "none" }}>
          <Camera
            onTakePhoto={dataUri => {
              this.onTakePhoto(dataUri);
            }}
            onCameraError={error => {
              this.onCameraError(error);
            }}
            idealFacingMode={FACING_MODES}
            idealResolution={{ width: 900, height: 500 }}
            imageType={IMAGE_TYPES.JPG}
            imageCompression={0.50}
            isMaxResolution={false}
            isImageMirror={false}
            isDisplayStartCameraError={true}
            sizeFactor={1}
            onCameraStart={stream => {
              this.onCameraStart(stream);
            }}
            onCameraStop={() => {
              this.onCameraStop();
            }}
          />
       
         <div className="loader"> </div>

          <div id="tips">
          <p>
           Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears.          
          </p>
     
          </div>
          <div id="options-or">
            <p className="or">OR</p>
          </div>
        </div>

        <div id="upload-photo-section" style={{ display: "none" }}>
          <div className="ios-pic">
            <div>
              <img className="display-img" id="pic" src="" alt=""/>
              <img className="display-img-url" id="photo" src="" alt="" />
            </div>

            <div className="upload-loader">  </div>

            <div>
              <label className="fileContainer">
                Use a Photo
                <input
                  id="browse-file"
                  type="file"
                  onClick={this.uploadSelfie}
                />
              </label>
             
            </div>
           
          </div>
        </div>


       <div>
        <p id="error-message" />
         </div>

          <div>
        <p id="success-message" />
         </div>

         <div id="responsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col>
            <div className="table-responsive">
            <table className="table" id="records_table" border='1'>
               <th align="center">ListUID</th>
               <th align="center">Faces Count</th>
               <th align="center">Owner</th>
               <th align="center">Created</th>
              </table>
              </div>

                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
                
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
         <Col></Col>
         <Col>
               <span className = "try-tab">
                     <a href="/#/show" onClick={() => window.location.reload()}>Back</a>
                </span>
          </Col>
          <Col></Col>
         </Row>
    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
                 
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/show" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>

    </div>

       
      </div>
    );
  }
}

export default Show;