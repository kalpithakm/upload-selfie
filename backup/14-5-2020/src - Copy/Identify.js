import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import jQuery from "jquery";
import {
    Row,
    Col,
    Input
} from "reactstrap";
import {
    Link
} from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.css";
import loader from './loader.gif';
import Header from './Header';
import { Cookies } from "react-cookie";
const cookies = new Cookies();
var ListUID,APIkey;

class Identify extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);

        this.state = {
        listname: cookies.get('cookieListUID'),
        apikey: cookies.get('cookieAPIkey')
     }

         var myapiCookie = cookies.get('cookieAPIkey')

    if (myapiCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
   console.log("APIkey", APIkey);
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }

  var myCookie = cookies.get('cookieListUID')
    if (myCookie != null) 
    {
  var x = cookies.get('cookieListUID');
   ListUID = x.key;
   console.log("ListUID", ListUID);
    }
   else
   {
     alert("Please enter ListUID");
     window.location.href='/#/editlist';
   }
  }

   
    uploadSelfie(e) {
        console.log(ListUID);
        jQuery(function($) {
            function readFile() {
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();

                    ImgfileReader.addEventListener("load", function(e) {
                        document.getElementById("photo").src = e.target.result;
                        var dataUri = e.target.result;

                        jQuery.ajax({
                            url: "http://20.20.20.21:8003/api/v2/face/identify/",
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: APIkey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri: dataUri,
                                listUID: ListUID,
                                attributes: "age,gender,smoker,ethnicity,alcohol,bmi,region,landmarks"

                            }),

                            success: function(response) {
                                jQuery("#error-message").hide();
                                console.log(response);

                                if (response) {
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("idn-tab").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";

                                    if (response.identified_faces) 
                                    {

                                    var jString = JSON.stringify(response.identified_faces);
                                    var jdata = JSON.parse(jString);
                                      var trHTML = '';
                                    for (var i = 0; i < jdata.length; i++) 
                                    {

                                      trHTML += 
                            '<tr><td>' +  '<img src= '+ jdata[i].matchDataUri +' />' + 
                   '</td><td>' + '<img src= '+ jdata[i].orgDataUri +' />' +
                   '</td><td>' + jdata[i].personId + 
                   '</td><td>' + jdata[i].confidencePercent +
                   '</td><td>' + jdata[i].confidenceGroup +
                   '</td></tr>';            
                      };
                   jQuery('#personrecords_table').append(trHTML);  
                                      

                                    }
                                    else
                                    {
                                     document.getElementById("responsecode").style.display = "block";
                                     document.getElementById("records_person").style.display = "none";
                                    }
                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader').show();
                                jQuery('#container-circles').hide();
                                jQuery('.react-html5-camera-photo').hide();

                            },
                            complete: function() {
                                jQuery('.upload-loader').hide();
                                jQuery('#container-circles').show();
                                jQuery('.react-html5-camera-photo').show();
                            },
                            error:function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("idn-tab").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }
                        });
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
    }

    render() {
        return ( 
<div className="Identify">
    <Header />
    
    <div id="identify-namepath">
        <Row>
            <Col></Col>
            <Col>
                <div className="idn-list" id="idn-tab">
                   
                    
                </div>
            </Col>
            <Col></Col>
        </Row>
    </div>
   
    <div id="upload-photo-section">
        <div className="ios-pic">
            <div>
                <img className="display-img" id="pic" src="" alt=""/>
                <img className="display-img-url" id="photo" src="" alt="" />
            </div>
            <div className="upload-loader"></div>
            <div>
                <label className="fileContainer">
                Use a Photo
                
                    <input
                  id="browse-file"
                  type="file"
                  onClick={this.uploadSelfie}
                />
                </label>
            </div>
        </div>
    </div>
    <div>
        <p id="error-message" />
    </div>
     <div>
        <p id="success-message" />
    </div>
    <div id="responsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col>
                <pre id="responsecode" style={{ display: "none" }} className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row id="records_person">
            
            <Col>
             <h5> Details </h5>
             <table id="personrecords_table" className="table persondetails"  border='1'>
              
            <tr>
              <th>Original Image </th>
              <th>Matched Image </th>
              <th>Person Id </th>
              <th>Person confidence Percent  </th>
              <th>Person confidence Group   </th>
            </tr>

               

               </table>
          </Col>
         
</Row>

         <Row className="refresh-btn">
         <Col></Col>
         <Col>
               <span className = "try-tab">
                     <a href="/#/identify" onClick={() => window.location.reload()}>Back</a>
                </span>
          </Col>
          <Col></Col>
         </Row>

    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/identify" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

    <div id="try-btn" style={{ display: "none" }}>
        <span className = "try-tab">
            <a href="/">Try Again</a>
        </span>
    </div>
</div>
    );
  }
}

export default Identify;