import React, { Component } from "react";
import "./Model.css";
import "./custom.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import { Container, Row, Col, Button, Input } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";

class Model extends Component {
  constructor(props) {
    super(props);
    this.uploadSelfie = this.uploadSelfie.bind(this);
  }

 
  onTakePhoto(dataUri) {
    // Do stuff with the dataUri photo...
    console.log("takePhoto");
    jQuery.ajax({
      url: "https://southeastasia.cognitive.sparshik.com/api/v1/face/detect/",
      method: "POST",
      crossDomain: true,
      headers: {
        Authorization: "Token b8b332467b1994253e4252448264e8b3946d3fb1",
        "Content-Type": "application/json"
      },
      data: JSON.stringify({
        dataUri: dataUri,
      }),
      success: function(response) {
          jQuery("#error-message").hide();
                        console.log(response);
                       
                     if (response) {
                           document.getElementById("responsetab").style.display = "block";
   
                            var codeString = JSON.stringify(response, null, 4);
                    jQuery('#responsecode').html(codeString);
                        }
       
       
      },

      beforeSend: function()
      {
       jQuery('.loader').show();
       jQuery('#container-circles').hide();
       jQuery('.react-html5-camera-photo').hide();

      },
      complete: function()
      {
       jQuery('.loader').hide();
       jQuery('#container-circles').show();
       jQuery('.react-html5-camera-photo').show();
      },
     
      error: function(response) {
                     if (response) {
                         jQuery("#error-message").show();
                     document.getElementById("error-message").innerHTML = '<p>' + response.responseJSON.error + "</p>";
                         
                        } else {
                             jQuery("#error-message").show();
                          console.log('error');
                        document.getElementById("error-message").innerHTML = '<p>Please try again, server error</p>';
                        }
                       
                    }
    });
  }

  uploadSelfie(e) {
 
    /* document.getElementById("upload-photo-section").style.display = "block";*/
    jQuery(function($) {
      function readFile() {
        if (this.files && this.files[0]) {
          var ImgfileReader = new FileReader();

          ImgfileReader.addEventListener("load", function(e) {
            document.getElementById("photo").src = e.target.result;
            var dataUri = e.target.result;

                jQuery.ajax({
      url: "https://southeastasia.cognitive.sparshik.com/api/v1/face/detect/",
      method: "POST",
      crossDomain: true,
      headers: {
        Authorization: "Token b8b332467b1994253e4252448264e8b3946d3fb1",
        "Content-Type": "application/json"
      },
      data: JSON.stringify({
        dataUri: dataUri,
      }),

        success:function(response) {
                   
                         jQuery("#error-message").hide();
                        console.log(response);
                       
                        if (response) {
                           document.getElementById("responsetab").style.display = "block";
   
                            var codeString = JSON.stringify(response, null, 4);
                            jQuery('#responsecode').html(codeString);
                        }
                      },

                       beforeSend: function()
      {
       jQuery('.loader').show();
       jQuery('#container-circles').hide();
       jQuery('.react-html5-camera-photo').hide();

      },
      complete: function()
      {
       jQuery('.loader').hide();
       jQuery('#container-circles').show();
       jQuery('.react-html5-camera-photo').show();
      },
                    error: function(response) {
                     if (response) {
                         jQuery("#error-message").show();
                     document.getElementById("error-message").innerHTML = '<p>' + response.responseJSON.error + "</p>";
                         
                        } else {
                             jQuery("#error-message").show();
                          console.log('error');
                        document.getElementById("error-message").innerHTML = '<p>Please try again, server error</p>';
                        }
                       
                    }
   
    });
          });

          ImgfileReader.readAsDataURL(this.files[0]);
        }
      }

      document
        .getElementById("browse-file")
        .addEventListener("change", readFile);
     
    });
  }

  onCameraError(error) {
    console.error("onCameraError");
    document.getElementById("video-section").style.display = "none";
    this.uploadSelfie();
  }

  onCameraStart(stream) {
    console.log("onCameraStart");
  }

  onCameraStop() {
    console.log("onCameraStop");
  }

  render() {
    return (
      <div className="Model">
        <div className="header" id="camera-head">
          <h2>TAKE A SELFIE to Identify</h2>
        </div>

         <div className="links-tag">
        <span className="links-tab active">
          <a href="/">Detect</a>
          </span>
          <span className="links-tab">
          <a href="/#/model">Identify</a>
          </span>
        </div>


          <div id="video-section">
          <Camera
            onTakePhoto={dataUri => {
              this.onTakePhoto(dataUri);
            }}
            onCameraError={error => {
              this.onCameraError(error);
            }}
            idealFacingMode={FACING_MODES}
            idealResolution={{ width: 900, height: 500 }}
            imageType={IMAGE_TYPES.JPG}
            imageCompression={0.50}
            isMaxResolution={false}
            isImageMirror={false}
            isDisplayStartCameraError={true}
            sizeFactor={1}
            onCameraStart={stream => {
              this.onCameraStart(stream);
            }}
            onCameraStop={() => {
              this.onCameraStop();
            }}
          />
       
         <div className="loader"> </div>

          <div id="tips">
          <p>
           Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears.          
          </p>
     
          </div>
          <div>
            <p className="or">OR</p>
          </div>
        </div>

        <div id="upload-photo-section">
          <div className="ios-pic">
            <div>
              <img className="display-img" id="pic" src="" alt=""/>
              <img className="display-img-url" id="photo" src="" alt="" />
            </div>

            <div className="upload-loader">  </div>

            <div>
              <label className="fileContainer">
                Use a Photo
                <input
                  id="browse-file"
                  type="file"
                  onClick={this.uploadSelfie}
                />
              </label>
             
            </div>
           
          </div>
        </div>

       <div>
        <p id="error-message" />
         </div>

         <div id="responsetab" style={{ display: "none" }}>
          <Row>
          <Col>
         </Col>
         
          <Col>
         <pre id="responsecode" className="x-code sparshik-demo-pre">
 
        </pre>

         </Col>
       
          <Col>
          </Col>
          </Row>

         </div>

       
      </div>
    );
  }
}

export default Model;