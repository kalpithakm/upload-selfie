import React, { Component } from "react";
import "./App.css";
import "./custom.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button, Input } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";

class Header extends Component {

  constructor(props) 
  {
    super(props);
   }

   componentDidMount()
   {
    jQuery("#headerlist").click(function() 
    {
     var element = document.getElementById("headerlist");

    });

   }


 render() {
    return (
  <div className="Header">
        <div className="header" id="camera-head">
          <h2>WELCOME TO FACE APIS</h2>
          <div className="edit-tab">
          <span className="edit-tab-list">
            <a href="/#/editapi">Add/Edit API Key</a>
          </span>
           <span className="edit-tab-list">
            <a href="/#/editlist">Add/Edit List UID</a>
          </span>
          </div>
        </div>

         <div className="links-tag">
          <span id="headerlist" className="links-tab">
            <a href="/">Home</a>
           </span>
         <span id="headerlist" className="links-tab">
             <a href="/#/list">Create List</a>
          </span>

          <span id="headerlist" className="links-tab">
              <a href="/#/show" >Show all lists</a>
          </span>

          <span id="headerlist" className="links-tab">
               <a href="/#/create">Add Face in the list</a>
          </span>

          <span id="headerlist" className="links-tab">
                <a href="/#/identify">Identify Face in the list</a>
          </span>

          <span id="headerlist" className="links-tab">
                <a href="/#/compare">Compare the documents </a>
          </span>
    
        </div>

        </div>

    	 );
       }
	}

export default Header;