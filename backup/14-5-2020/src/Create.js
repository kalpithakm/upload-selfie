import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import jQuery from "jquery";
import {
    Row,
    Col,
    Input
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import Header from './Header';
import { Cookies } from "react-cookie";
import config from './config';
const cookies = new Cookies();
var ListUID,APIkey;

class Create extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);

    this.state = {
        listname: cookies.get('cookieListUID'),
        apikey: cookies.get('cookieAPIkey')
     }

     var myapiCookie = cookies.get('cookieAPIkey')

    if (myapiCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
   
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }

 var myCookie = cookies.get('cookieListUID')
    if (myCookie != null) 
    {
  var xy = cookies.get('cookieListUID');
 
   ListUID = xy.key;
  
    }
   else if (myCookie == null) 
   {
     alert("Please enter ListUID");
     window.location.href='/#/editlist';
   }


  }


    uploadSelfie(e) {
       
        var uploadtextname = document.getElementById("identifyname").value;

        /* document.getElementById("upload-photo-section").style.display = "block";*/
        jQuery(function($) {
            function readFile() {
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();

                    ImgfileReader.addEventListener("load", function(e) {
                        document.getElementById("photo").src = e.target.result;
                        var dataUri = e.target.result;


                        jQuery.ajax({
                            url: config.add_face_api,
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: APIkey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri: dataUri,
                                personId: uploadtextname,
                                listUID: ListUID
                            }),

                            success: function(response) {

                                jQuery("#error-message").hide();
                                

                                if (response) {
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("idn-tab").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";
                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader').show();
                                jQuery('#container-circles').hide();
                                jQuery('.react-html5-camera-photo').hide();

                            },
                            complete: function() {
                                jQuery('.upload-loader').hide();
                                jQuery('#container-circles').show();
                                jQuery('.react-html5-camera-photo').show();
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);              

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }

                        });
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
    }

   
    render() {
        return ( 
            <div className = "Create" >
            <Header />

            <div id = "identify-namepath" >
            <Row >
            <Col >
            </Col>

            <Col >
            <div className = "idn-list"
            id = "idn-tab">
           <h5 > Please Enter the below details </h5>

            <span className = "idn-input">
            <label> Face Name </label>  
                <Input type = "text"
            id = "identifyname">
            </Input> 
            </span>

            </div>
            </Col>

            <Col >
            </Col>

            </Row> 
            </div>

            <div id = "upload-photo-section">
            <div className = "ios-pic">
            <div >
            <img className = "display-img"
            id = "pic"
            src = ""
            alt = ""/>
            <img className = "display-img-url"
            id = "photo"
            src = ""
            alt = ""/>
            </div>

            <div className = "upload-loader" > </div>

            <div >
            <label className = "fileContainer">
            Use a Photo <
            input id = "browse-file"
            type = "file"
            onClick = {
                this.uploadSelfie
            }
            /> 
            </label>

            </div>

            </div> </div>

            <div>
            <p id = "error-message"> </p>
            </div>

            <div>
                 <p id="success-message"></p>
            </div>

              <div id="responsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
         <Col></Col>
         <Col>
               <span className = "try-tab">
                     <a href="/#/create" onClick={() => window.location.reload()}>Back</a>
                </span>
          </Col>
          <Col></Col>
         </Row>
    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col>
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/create" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

            <div id = "try-btn"
            style = {
                {
                    display: "none"
                }
            } >
            <span className = "try-tab">
            <a href = "/" > Try Again </a> 
            </span> </div>
</div>
        );
    }
}

export default Create;