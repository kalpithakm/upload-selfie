
/*var config =
{
"add_face_api":"http://20.20.20.21:8002/api/v1/facelists/addFace/",
"identify_face_api": "http://20.20.20.21:8002/api/v2/face/identify/",
"compare_face_api": "http://20.20.20.21:8002/api/v1/face/compare/image/",
"show_list_api": "http://20.20.20.21:8002/api/v1/facelists/",
"create_list_api": "http://20.20.20.21:8002/api/v1/facelists/"
};*/

var config =
{
"add_face_api":"https://southeastasia.cognitive.sparshik.com/api/v1/facelists/addFace/",
"identify_face_api": "https://southeastasia.cognitive.sparshik.com/api/v2/face/identify/",
"compare_face_api": "https://southeastasia.cognitive.sparshik.com/api/v1/face/compare/image/",
"show_list_api": "https://southeastasia.cognitive.sparshik.com/api/v1/facelists/",
"create_list_api": "https://southeastasia.cognitive.sparshik.com/api/v1/facelists/"
};



module.exports = config;
