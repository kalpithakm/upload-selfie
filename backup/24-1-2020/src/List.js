import React, { Component } from "react";
import "./App.css";
import "./custom.css";
import jQuery from "jquery";
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button, Input } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import Header from './Header';
import { Cookies } from "react-cookie";
const cookies = new Cookies();
var APIkey;

class List extends Component {
  constructor(props) {
    super(props);

     this.state = {
        apikey: cookies.get('cookieAPIkey')
     }
    var myCookie = cookies.get('cookieAPIkey')

    if (myCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
   console.log("APIkey", APIkey);
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }

  }





  componentDidMount(){
 jQuery("#createlist").click(function() {
 console.log(APIkey);
   jQuery.ajax({
                url:
                  "https://southeastasia.cognitive.sparshik.com/api/v1/facelists/",
                headers: {
                  Authorization:
                    APIkey,
                  "Content-Type": "application/json"
                },
                method: "PUT",
                crossDomain: true,
                

                success: function(response) {
                  if (response) {
                    jQuery("#error-message").hide();
                   document.getElementById("idn-tab").style.display = "none";
                    document.getElementById("errorresponsetab").style.display = "none";
                    document.getElementById("responsetab").style.display = "block";
                    
                    var codeString = JSON.stringify(response, null, 4);
                    jQuery('#responsecode').html(codeString);
                    console.log(codeString);
                    document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";
                  
                  }
                },

               error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("idn-tab").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }

              });
    });

      }


  render() {
    return (
      <div className="List">
        <Header />
        
        <div id="identify-namepath">
        <Row>
           <Col>
           </Col>

           <Col>
          
           <div className="idn-list" id="idn-tab">


             <Button id="createlist"> Please Click Here to Create List </Button>
             </div>

              <span className="idn-input" style={{ display: "none" }}> 
             <label>Face List UID</label>      
              <Input type="text" id="listname">
                </Input>
            </span>
            
                 </Col>

                 <Col>
                </Col>

                </Row>
        </div>



        <div id="upload-photo-section" style={{ display: "none" }}>
          <div className="ios-pic">
            <div>
              <img className="display-img" id="pic" src="" alt=""/>
              <img className="display-img-url" id="photo" src="" alt="" />
            </div>

            <div className="upload-loader">  </div>

            <div>
              <label className="fileContainer">
                Use a Photo
                <input
                  id="browse-file"
                  type="file"
                  onClick={this.uploadSelfie}
                />
              </label>
             
            </div>
           
          </div>
        </div>


         <div>
        <p id="error-message" />
         </div>

         <div>
        <p id="success-message" />
        </div>

         <div id="responsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
         <Col></Col>
         <Col>
               <span className = "try-tab">
                     <a href="/#/list" onClick={() => window.location.reload()}>Back</a>
                </span>
          </Col>
          <Col></Col>
         </Row>
    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/list" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

       
      </div>
    );
  }
}

export default List;