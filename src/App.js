import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import "bootstrap/dist/css/bootstrap.css";
import Header from './Header';

class App extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);
    }


   

    uploadSelfie(e) {
        document.getElementById("video-section").style.display = "none";
        document.getElementById("tips").style.display = "none";
        document.getElementById("options-or").style.display = "none";

        var uploadtextname = document.getElementById("identifyname").value;
        console.log(uploadtextname);
        var uploadlistuid = document.getElementById("listname").value;
        console.log(uploadlistuid);

          var uploadapikey = document.getElementById("apikey").value;
        console.log(uploadapikey);

        /* document.getElementById("upload-photo-section").style.display = "block";*/
        jQuery(function($) {
            function readFile() {
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();

                    ImgfileReader.addEventListener("load", function(e) {
                        document.getElementById("photo").src = e.target.result;
                        var dataUri = e.target.result;


                        jQuery.ajax({
                            url: "https://southeastasia.cognitive.sparshik.com/api/v1/facelists/addFace/",
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: uploadapikey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri: dataUri,
                                identifier: uploadtextname,
                                listUID: uploadlistuid
                            }),

                            success: function(response) {

                                jQuery("#error-message").hide();
                                console.log(response);

                                if (response) {
                                    document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("idn-tab").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";
                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader').show();
                                jQuery('#container-circles').hide();
                                jQuery('.react-html5-camera-photo').hide();

                            },
                            complete: function() {
                                jQuery('.upload-loader').hide();
                                jQuery('#container-circles').show();
                                jQuery('.react-html5-camera-photo').show();
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }

                        });
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
    }


    render() {
        return ( 
            <div className = "App">
            <Header />

     <div className="intro">
             <h5>FaceAPIs to detect face features, verify and update</h5>
             <div className="home-para">
             <a href="https://documenter.getpostman.com/view/6254805/SWLcc8Ns?version=latest" rel="noopener noreferrer" target="_blank"> API Documentation </a>
             </div>
     </div>

            </div>
        );
    }
}

export default App;